<?php

/**
 * Posts Routes
 */
$route->get('/posts', 'PostsController@idnex');
$route->get('/posts/{id}', 'PostsController@find');
$route->post('/posts', 'PostsController@create');
$route->patch('/posts/{id}', 'PostsController@update');
$route->delete('/posts/{id}', 'PostsController@delete');


/**
 * Comments Routes
 */
$route->get('/comments', 'CommentsController@idnex');
$route->get('/comments/{id}', 'CommentsController@find');
$route->post('/comments', 'CommentsController@create');
$route->patch('/comments/{id}', 'CommentsController@update');
$route->delete('/comments/{id}', 'CommentsController@delete');