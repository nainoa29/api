<?php
namespace Core\Routing;

use Core\Request;
use Core\Routing\RouteCollection;

class Router {

    /**
     * @var
     */
    protected $availableRoutes;

    /**
     * The request object
     *
     * @var
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->collectRoutes();
    }

    /**
     * @var
     */
    protected $routesCollection;


    /**
     * @param Request $request
     */
    public function validate(Request $request)
    {
        //@TODO to wildcard matching
    }

    /**
     *
     */
    private function collectRoutes()
    {
        $route = new RouteCollection;
        require_once 'routes.php';

        $this->availableRoutes = $route->getRoutes();
    }


    private function wildcardMatching()
    {

    }



}