<?php
namespace Core\Routing;

class RouteCollection {

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var
     */
    protected $routesCollection;

    /**
     * @param string $uri
     * @param string $controllerAction
     * @throws Exception
     */
    public function get($uri = '', $controllerAction = '')
    {
        $this->addRoute('GET', $uri, $controllerAction);
    }

    /**
     * @param string $uri
     * @param string $controllerAction
     * @throws Exception
     */
    public function post($uri = '', $controllerAction = '')
    {
        $this->addRoute('POST', $uri, $controllerAction);
    }

    /**
     * @param string $uri
     * @param string $controllerAction
     * @throws Exception
     */
    public function patch($uri = '', $controllerAction = '')
    {
        $this->addRoute('PATCH', $uri, $controllerAction);
    }

    /**
     * @param string $uri
     * @param string $controllerAction
     * @throws Exception
     */
    public function delete($uri = '', $controllerAction = '')
    {
        $this->addRoute('DELETE', $uri, $controllerAction);
    }

    /**
     * Returns collection of routes
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param $method
     * @param $uri
     * @param $controllerAction
     * @return bool
     */
    private function validateRouteParams($method, $uri, $controllerAction)
    {
        if(empty($controllerAction)) {
            die;
        }

        return true;
    }

    /**
     * @param string $method
     * @param $uri
     * @param $controllerAction
     * @return bool
     * @throws Exception
     */
    private function addRoute($method = 'GET', $uri, $controllerAction)
    {
        if($this->validateRouteParams($method, $uri, $controllerAction)) {
            if(!array_key_exists($method, $this->routes)) {
                $this->routes[$method] = [];
            }

            $this->routes[$method] = [
                'uri' => $uri,
                'controller' => $controllerAction,
                'action' => $this->extractAction($controllerAction)
            ];
        }

        return false;
    }

    /**
     * @param string $controllerAction
     * @return bool
     */
    private function extractAction($controllerAction = '')
    {
       if(!empty($controllerAction)) {
           $parts = explode('@', $controllerAction);

           if(isset($parts[1])) {
               return $parts[1];
           } else {
               return false;
           }
       }

       return false;
    }

}