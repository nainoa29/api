<?php
namespace Core;

class Request {

    /**
     * The request payload
     *
     * @var
     */
    protected $payload;

    /**
     * The request method
     *
     * @var
     */
    protected $requestMethod;

    /**
     * The request URI
     *
     * @var
     */
    protected $uri;

    public function __construct()
    {
        $this->boot();
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * loads necessary properties/dependencies
     */
    private function boot()
    {
        $this->setUri();
        $this->setRequestMethod();
        $this->setPayload();
    }

    /**
     * Sets the request method
     */
    private function setRequestMethod()
    {
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Sets the payload
     */
    private function setPayload()
    {
        if(in_array($this->requestMethod, ['POST', 'PATCH'])) {
            $this->payload = json_decode(file_get_contents('php://input'));
        }
    }

    /**
     * @return string
     */
    private function setUri()
    {
        return strtok(trim($_SERVER['REQUEST_URI'], '/'), '?');
    }

}