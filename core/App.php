<?php
namespace Core;

class App {

    /**
     * @var
     */
    protected $router;

    /**
     * @var
     */
    protected $request;

    public function __construct()
    {
        $this->boot();
    }

    /**
     *
     */
    public function run()
    {
        /**
         * Validates the request
         */
        $this->router->validate(
            $this->request
        );
    }

    /**
     *
     */
    private function boot()
    {
        $this->initRequest();

        $this->initRouter();
    }

    /**
     *
     */
    private function initRequest()
    {
        $this->request = new Request;
    }

    /**
     *
     */
    private function initRouter()
    {
        $this->router = new Router($this->request);
    }

}