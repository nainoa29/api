<?php
define('DS', DIRECTORY_SEPARATOR);

spl_autoload_register(function($class) {
    $file = $class.'.php';

    if(file_exists(__DIR__. DS .str_replace('\\', DS, $file))) {
        require_once __DIR__. DS .str_replace('\\', DS, $file);
    }
});
